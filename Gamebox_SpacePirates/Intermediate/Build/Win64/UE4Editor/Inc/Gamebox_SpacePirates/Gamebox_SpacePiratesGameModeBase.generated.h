// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEBOX_SPACEPIRATES_Gamebox_SpacePiratesGameModeBase_generated_h
#error "Gamebox_SpacePiratesGameModeBase.generated.h already included, missing '#pragma once' in Gamebox_SpacePiratesGameModeBase.h"
#endif
#define GAMEBOX_SPACEPIRATES_Gamebox_SpacePiratesGameModeBase_generated_h

#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_SPARSE_DATA
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_RPC_WRAPPERS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGamebox_SpacePiratesGameModeBase(); \
	friend struct Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGamebox_SpacePiratesGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AGamebox_SpacePiratesGameModeBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGamebox_SpacePiratesGameModeBase(); \
	friend struct Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGamebox_SpacePiratesGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AGamebox_SpacePiratesGameModeBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGamebox_SpacePiratesGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGamebox_SpacePiratesGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamebox_SpacePiratesGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamebox_SpacePiratesGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamebox_SpacePiratesGameModeBase(AGamebox_SpacePiratesGameModeBase&&); \
	NO_API AGamebox_SpacePiratesGameModeBase(const AGamebox_SpacePiratesGameModeBase&); \
public:


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGamebox_SpacePiratesGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamebox_SpacePiratesGameModeBase(AGamebox_SpacePiratesGameModeBase&&); \
	NO_API AGamebox_SpacePiratesGameModeBase(const AGamebox_SpacePiratesGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamebox_SpacePiratesGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamebox_SpacePiratesGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGamebox_SpacePiratesGameModeBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_12_PROLOG
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_RPC_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_INCLASS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<class AGamebox_SpacePiratesGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Gamebox_SpacePiratesGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
