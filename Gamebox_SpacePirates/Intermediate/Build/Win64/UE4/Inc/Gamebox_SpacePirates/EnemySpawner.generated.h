// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEBOX_SPACEPIRATES_EnemySpawner_generated_h
#error "EnemySpawner.generated.h already included, missing '#pragma once' in EnemySpawner.h"
#endif
#define GAMEBOX_SPACEPIRATES_EnemySpawner_generated_h

#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_SPARSE_DATA
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_RPC_WRAPPERS \
	virtual void TriggerNewWave_Implementation(); \
 \
	DECLARE_FUNCTION(execTriggerNewWave) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TriggerNewWave_Implementation(); \
		P_NATIVE_END; \
	}


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTriggerNewWave) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TriggerNewWave_Implementation(); \
		P_NATIVE_END; \
	}


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_EVENT_PARMS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_CALLBACK_WRAPPERS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemySpawner(); \
	friend struct Z_Construct_UClass_AEnemySpawner_Statics; \
public: \
	DECLARE_CLASS(AEnemySpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AEnemySpawner)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAEnemySpawner(); \
	friend struct Z_Construct_UClass_AEnemySpawner_Statics; \
public: \
	DECLARE_CLASS(AEnemySpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AEnemySpawner)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemySpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemySpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemySpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemySpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemySpawner(AEnemySpawner&&); \
	NO_API AEnemySpawner(const AEnemySpawner&); \
public:


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemySpawner(AEnemySpawner&&); \
	NO_API AEnemySpawner(const AEnemySpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemySpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemySpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemySpawner)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_PRIVATE_PROPERTY_OFFSET
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_10_PROLOG \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_EVENT_PARMS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_RPC_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_CALLBACK_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_INCLASS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_CALLBACK_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_INCLASS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<class AEnemySpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemySpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
