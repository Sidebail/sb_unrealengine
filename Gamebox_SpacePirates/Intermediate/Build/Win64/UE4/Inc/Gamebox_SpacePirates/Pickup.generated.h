// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMEBOX_SPACEPIRATES_Pickup_generated_h
#error "Pickup.generated.h already included, missing '#pragma once' in Pickup.h"
#endif
#define GAMEBOX_SPACEPIRATES_Pickup_generated_h

#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_SPARSE_DATA
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_RPC_WRAPPERS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public:


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickup)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_PRIVATE_PROPERTY_OFFSET
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_18_PROLOG
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_RPC_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_INCLASS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_INCLASS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<class APickup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamebox_SpacePirates_Source_Gamebox_SpacePirates_Pickup_h


#define FOREACH_ENUM_EPICKUPTYPE(op) \
	op(EPickupType::ROCKET) \
	op(EPickupType::HEALTH) \
	op(EPickupType::AIMBOT) \
	op(EPickupType::SHIELD) \
	op(EPickupType::SPEED) \
	op(EPickupType::TRIPLET) 

enum class EPickupType : uint8;
template<> GAMEBOX_SPACEPIRATES_API UEnum* StaticEnum<EPickupType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
