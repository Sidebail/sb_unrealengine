// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FRotator;
#ifdef GAMEBOX_SPACEPIRATES_EnemyBase_generated_h
#error "EnemyBase.generated.h already included, missing '#pragma once' in EnemyBase.h"
#endif
#define GAMEBOX_SPACEPIRATES_EnemyBase_generated_h

#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_SPARSE_DATA
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_RPC_WRAPPERS \
	virtual void RecieveDamage_Implementation(FVector HitLocation, FRotator ProjectileRotation); \
 \
	DECLARE_FUNCTION(execRecieveDamage) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_HitLocation); \
		P_GET_STRUCT(FRotator,Z_Param_ProjectileRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RecieveDamage_Implementation(Z_Param_HitLocation,Z_Param_ProjectileRotation); \
		P_NATIVE_END; \
	}


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRecieveDamage) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_HitLocation); \
		P_GET_STRUCT(FRotator,Z_Param_ProjectileRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RecieveDamage_Implementation(Z_Param_HitLocation,Z_Param_ProjectileRotation); \
		P_NATIVE_END; \
	}


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_EVENT_PARMS \
	struct EnemyBase_eventRecieveDamage_Parms \
	{ \
		FVector HitLocation; \
		FRotator ProjectileRotation; \
	};


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_CALLBACK_WRAPPERS
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamebox_SpacePirates"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public:


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyBase)


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_PRIVATE_PROPERTY_OFFSET
#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_13_PROLOG \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_EVENT_PARMS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_RPC_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_CALLBACK_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_INCLASS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_SPARSE_DATA \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_CALLBACK_WRAPPERS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_INCLASS_NO_PURE_DECLS \
	Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<class AEnemyBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamebox_SpacePirates_Source_Gamebox_SpacePirates_EnemyBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
