// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gamebox_SpacePirates/PlayerBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerBase() {}
// Cross Module References
	GAMEBOX_SPACEPIRATES_API UClass* Z_Construct_UClass_APlayerBase_NoRegister();
	GAMEBOX_SPACEPIRATES_API UClass* Z_Construct_UClass_APlayerBase();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_Gamebox_SpacePirates();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GAMEBOX_SPACEPIRATES_API UClass* Z_Construct_UClass_AProjectileBase_NoRegister();
// End Cross Module References
	void APlayerBase::StaticRegisterNativesAPlayerBase()
	{
	}
	UClass* Z_Construct_UClass_APlayerBase_NoRegister()
	{
		return APlayerBase::StaticClass();
	}
	struct Z_Construct_UClass_APlayerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerProjectile_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_PlayerProjectile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimedScoreDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimedScoreDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PassiveScoreAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PassiveScoreAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RechargeDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RechargeDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ammo_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Ammo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FireDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AccelerationRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AccelerationRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlowdownRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SlowdownRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TurningSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TurningSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinimumSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MovementSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Health;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Gamebox_SpacePirates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlayerBase.h" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_PlayerProjectile_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "Comment", "//Projectile, that will be spawned on Fire\n" },
		{ "ModuleRelativePath", "PlayerBase.h" },
		{ "ToolTip", "Projectile, that will be spawned on Fire" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_PlayerProjectile = { "PlayerProjectile", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, PlayerProjectile), Z_Construct_UClass_AProjectileBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_PlayerProjectile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_PlayerProjectile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_TimedScoreDelay_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "Comment", "// A delay that determines how frequently Player gets passive score amount\n" },
		{ "ModuleRelativePath", "PlayerBase.h" },
		{ "ToolTip", "A delay that determines how frequently Player gets passive score amount" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_TimedScoreDelay = { "TimedScoreDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, TimedScoreDelay), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_TimedScoreDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_TimedScoreDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_PassiveScoreAmount_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "Comment", "//Player recieves score passively!\n" },
		{ "ModuleRelativePath", "PlayerBase.h" },
		{ "ToolTip", "Player recieves score passively!" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_PassiveScoreAmount = { "PassiveScoreAmount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, PassiveScoreAmount), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_PassiveScoreAmount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_PassiveScoreAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_RechargeDelay_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_RechargeDelay = { "RechargeDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, RechargeDelay), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_RechargeDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_RechargeDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_Ammo_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_Ammo = { "Ammo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, Ammo), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_Ammo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_Ammo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_FireDelay_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_FireDelay = { "FireDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, FireDelay), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_FireDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_FireDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_AccelerationRate_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_AccelerationRate = { "AccelerationRate", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, AccelerationRate), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_AccelerationRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_AccelerationRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_SlowdownRate_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_SlowdownRate = { "SlowdownRate", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, SlowdownRate), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_SlowdownRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_SlowdownRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_TurningSpeed_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_TurningSpeed = { "TurningSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, TurningSpeed), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_TurningSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_TurningSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_MinimumSpeed_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_MinimumSpeed = { "MinimumSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, MinimumSpeed), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_MinimumSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_MinimumSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_MaximumSpeed_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_MaximumSpeed = { "MaximumSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, MaximumSpeed), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_MaximumSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_MaximumSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_MovementSpeed_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "ModuleRelativePath", "PlayerBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_MovementSpeed = { "MovementSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, MovementSpeed), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_MovementSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_MovementSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerBase_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "PlayerBase" },
		{ "Comment", "// Player's variables: Health, Movement Speed, TurningSpeed, FireDelay, Ammo, RechargeDelay and Score\n// Also, variables for smooth acceleration based movement: Maximum Speed, Minimum Speed, SlowdownRate and AccelerationRate\n" },
		{ "ModuleRelativePath", "PlayerBase.h" },
		{ "ToolTip", "Player's variables: Health, Movement Speed, TurningSpeed, FireDelay, Ammo, RechargeDelay and Score\nAlso, variables for smooth acceleration based movement: Maximum Speed, Minimum Speed, SlowdownRate and AccelerationRate" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APlayerBase_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayerBase, Health), METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::NewProp_Health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::NewProp_Health_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayerBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_PlayerProjectile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_TimedScoreDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_PassiveScoreAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_RechargeDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_Ammo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_FireDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_AccelerationRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_SlowdownRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_TurningSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_MinimumSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_MaximumSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_MovementSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerBase_Statics::NewProp_Health,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlayerBase_Statics::ClassParams = {
		&APlayerBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APlayerBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlayerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlayerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerBase, 1360658190);
	template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<APlayerBase>()
	{
		return APlayerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerBase(Z_Construct_UClass_APlayerBase, &APlayerBase::StaticClass, TEXT("/Script/Gamebox_SpacePirates"), TEXT("APlayerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
