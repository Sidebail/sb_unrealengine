// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gamebox_SpacePirates/Gamebox_SpacePiratesGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGamebox_SpacePiratesGameModeBase() {}
// Cross Module References
	GAMEBOX_SPACEPIRATES_API UClass* Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_NoRegister();
	GAMEBOX_SPACEPIRATES_API UClass* Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Gamebox_SpacePirates();
// End Cross Module References
	void AGamebox_SpacePiratesGameModeBase::StaticRegisterNativesAGamebox_SpacePiratesGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_NoRegister()
	{
		return AGamebox_SpacePiratesGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPlayerDead_MetaData[];
#endif
		static void NewProp_bIsPlayerDead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPlayerDead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BossKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BossKilled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EliteKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EliteKilled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FastKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FastKilled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BigKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BigKilled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimpleKilled_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SimpleKilled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeSurvived_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeSurvived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Score_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Score;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Highscore_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Highscore;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Gamebox_SpacePirates,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "Comment", "// Is Player Dead varaible checks if player is... you know, dead\n" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ToolTip", "Is Player Dead varaible checks if player is... you know, dead" },
	};
#endif
	void Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead_SetBit(void* Obj)
	{
		((AGamebox_SpacePiratesGameModeBase*)Obj)->bIsPlayerDead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead = { "bIsPlayerDead", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGamebox_SpacePiratesGameModeBase), &Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BossKilled_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BossKilled = { "BossKilled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, BossKilled), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BossKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BossKilled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_EliteKilled_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_EliteKilled = { "EliteKilled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, EliteKilled), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_EliteKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_EliteKilled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_FastKilled_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_FastKilled = { "FastKilled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, FastKilled), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_FastKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_FastKilled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BigKilled_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BigKilled = { "BigKilled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, BigKilled), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BigKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BigKilled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_SimpleKilled_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "Comment", "//Storing Enemy Killed Counters\n" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ToolTip", "Storing Enemy Killed Counters" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_SimpleKilled = { "SimpleKilled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, SimpleKilled), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_SimpleKilled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_SimpleKilled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_TimeSurvived_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "Comment", "//Storing the time that player survived, for stats\n" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ToolTip", "Storing the time that player survived, for stats" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_TimeSurvived = { "TimeSurvived", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, TimeSurvived), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_TimeSurvived_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_TimeSurvived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Score_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Score = { "Score", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, Score), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Score_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Score_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Highscore_MetaData[] = {
		{ "Category", "Gamebox_SpacePiratesGameModeBase" },
		{ "Comment", "//Storing the score value in GameMode, as well as Highscore\n" },
		{ "ModuleRelativePath", "Gamebox_SpacePiratesGameModeBase.h" },
		{ "ToolTip", "Storing the score value in GameMode, as well as Highscore" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Highscore = { "Highscore", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGamebox_SpacePiratesGameModeBase, Highscore), METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Highscore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Highscore_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_bIsPlayerDead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BossKilled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_EliteKilled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_FastKilled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_BigKilled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_SimpleKilled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_TimeSurvived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Score,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::NewProp_Highscore,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGamebox_SpacePiratesGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::ClassParams = {
		&AGamebox_SpacePiratesGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGamebox_SpacePiratesGameModeBase, 4085862636);
	template<> GAMEBOX_SPACEPIRATES_API UClass* StaticClass<AGamebox_SpacePiratesGameModeBase>()
	{
		return AGamebox_SpacePiratesGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGamebox_SpacePiratesGameModeBase(Z_Construct_UClass_AGamebox_SpacePiratesGameModeBase, &AGamebox_SpacePiratesGameModeBase::StaticClass, TEXT("/Script/Gamebox_SpacePirates"), TEXT("AGamebox_SpacePiratesGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGamebox_SpacePiratesGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
