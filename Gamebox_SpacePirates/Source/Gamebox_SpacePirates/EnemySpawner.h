// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

class AEnemyBase;
UCLASS()
class GAMEBOX_SPACEPIRATES_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();

	// Setting variables for spawner to work: Array with possible Enemy types, Array with actors, location and rotation of which will be used for spawning enemies
	// Including Spawn delay, time that spawner will wait before spawning the new wave
	// And Tier index. Tier index will Increment based on Player's score and will cause new enemy types to spawn and SpawnDelay to get shortened
	// As well as TierThreshold arraym which will determine how much score player need to proceed to next Tier
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<TSubclassOf<AEnemyBase>> EnemiesArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<AActor*> SpawnPointsArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SpawnDelay;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 TierIndex;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<int32> TierThersholds;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Function that spawns the new wave of enemies
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void TriggerNewWave();
	void TriggerNewWave_Implementation();

};
