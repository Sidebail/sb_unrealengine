// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerBase.generated.h"

class AProjectileBase;

UCLASS()
class GAMEBOX_SPACEPIRATES_API APlayerBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerBase();

	// Player's variables: Health, Movement Speed, TurningSpeed, FireDelay, Ammo, RechargeDelay and Score
	// Also, variables for smooth acceleration based movement: Maximum Speed, Minimum Speed, SlowdownRate and AccelerationRate
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Health;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MovementSpeed; // ���������� ������� �������� ������������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MaximumSpeed; // ������������ �������� ������������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float MinimumSpeed; // ����������� �������� ������������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float TurningSpeed; // �������� ��������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float SlowdownRate; // �������� ����������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float AccelerationRate; // �������� ���������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float FireDelay; // ��� ����� �� ����� ��������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Ammo; // ����������
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float RechargeDelay; // ��� ����� �� �������� +1 ���������

	//Player recieves score passively!
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 PassiveScoreAmount;

	// A delay that determines how frequently Player gets passive score amount
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float TimedScoreDelay;

	//Projectile, that will be spawned on Fire
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf <AProjectileBase> PlayerProjectile;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
