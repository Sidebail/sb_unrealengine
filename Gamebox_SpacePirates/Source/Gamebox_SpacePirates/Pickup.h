// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UENUM(BlueprintType)
enum class EPickupType : uint8 {
	ROCKET,
	HEALTH,
	AIMBOT,
	SHIELD,
	SPEED,
	TRIPLET
};
UCLASS()
class GAMEBOX_SPACEPIRATES_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	// The delay variable, that will set the time while this pickup is operational
	// IF SET TO ZERO, THEN UPGRADE IS FOREVER!
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float WorkingTime = 0;

	//Variables that set the movement direction: movement vector and speed
	UPROPERTY(BlueprintReadWrite)
		FVector MovementVector = FVector(0,0,0);
	UPROPERTY(BlueprintReadWrite)
		float Speed = 0;

	// Variable that sets the lifetime of pickup in seconds
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float LifeTime = 5;

	// Enumerator to determine the upgrade type
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		EPickupType PickupType;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
