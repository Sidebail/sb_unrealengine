// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Gamebox_SpacePiratesGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEBOX_SPACEPIRATES_API AGamebox_SpacePiratesGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	//Storing the score value in GameMode, as well as Highscore

	UPROPERTY(BlueprintReadWrite)
		int32 Highscore;
	
	UPROPERTY(BlueprintReadWrite)
		int32 Score;

	//Storing the time that player survived, for stats
	UPROPERTY(BlueprintReadWrite)
		float TimeSurvived;

	//Storing Enemy Killed Counters
	UPROPERTY(BlueprintReadWrite)
		int32 SimpleKilled;
	UPROPERTY(BlueprintReadWrite)
		int32 BigKilled;
	UPROPERTY(BlueprintReadWrite)
		int32 FastKilled;
	UPROPERTY(BlueprintReadWrite)
		int32 EliteKilled;
	UPROPERTY(BlueprintReadWrite)
		int32 BossKilled;

	// Is Player Dead varaible checks if player is... you know, dead
	UPROPERTY(BlueprintReadWrite)
		bool bIsPlayerDead;
	
};
