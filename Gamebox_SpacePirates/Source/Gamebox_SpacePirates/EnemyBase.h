// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyBase.generated.h"

// Class forwarding
class AProjectileBase;
class APickup;

UCLASS()
class GAMEBOX_SPACEPIRATES_API AEnemyBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyBase();
	
	//Enemy's basic variables: Health, Speed, Damage, AttackDelay, ScoreValue
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Health;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float Speed;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Damage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float AttackDelay;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 ScoreValue;

	//Projectile, that could be spawned on Fire
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf <AProjectileBase> PlayerProjectile;

	// Two arrays that moderate what pickups enemy will drop on death and how rare they appear
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<TSubclassOf <APickup>> PickupsArray;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<int32> ProbabilitiesArray;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void RecieveDamage(FVector HitLocation, FRotator ProjectileRotation);
	void RecieveDamage_Implementation(FVector HitLocation, FRotator ProjectileRotation);

};
