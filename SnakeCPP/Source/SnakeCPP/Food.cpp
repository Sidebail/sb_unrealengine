// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "CPP_SnakeBase.h"
#include "FoodSpawner.h"
#include "SnakeCPPGameModeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* instigator, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ACPP_SnakeBase>(instigator);
		if(IsValid(Snake)){
			Snake->AddSnakeElement();
			if (IsValid(foodCreator)) {
				foodCreator->SpawnNewFood();
			}
			this->Destroy();
		}
	}
}

void AFood::ToggleCollision()
{
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

