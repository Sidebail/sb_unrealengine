// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "FoodSpawner.h"

// Sets default values
ACPP_SnakeBase::ACPP_SnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementTimeInterval = 0.5f;
	StartSnakeSize = 4;
	LastMoveDirection = EMovementDirection::DOWN;
	LastConfirmedMoveDirection = EMovementDirection::DOWN;
	
}

// Called when the game starts or when spawned
void ACPP_SnakeBase::BeginPlay()
{
	Super::BeginPlay();
	//SetActorTickInterval(MovementTimeInterval);
	AddSnakeElement(StartSnakeSize);
	/* Activate the Move timer */
	GetWorld()->GetTimerManager().SetTimer(MoveTimerHandle, this, &ACPP_SnakeBase::Move, MovementTimeInterval, true);
}

// Called every frame
void ACPP_SnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
/*
	Creating and adding the Snake Element to the array of elements
*/
void ACPP_SnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {

		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, -100);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		NewSnakeElem->SnakeOwner = this;
		int32 NewElementIndex = SnakeElements.Add(NewSnakeElem);
		if (NewElementIndex == 0) {
			NewSnakeElem->SetFirstElementType();
			NewSnakeElem->SetActorLocation(FVector(0, 0, 0));
		}
		

	}
}

void ACPP_SnakeBase::Move()
{
	
	FVector MovementVector = FVector(0,0,0);
	
	float MovementSpeed = ElementSize;

	switch (LastMoveDirection) {
	case EMovementDirection::UP:
		if (LastConfirmedMoveDirection != EMovementDirection::DOWN) {
			MovementVector.X = MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::UP;
		}
		else {
			MovementVector.X = -MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::DOWN;
		}
		break;
	case EMovementDirection::DOWN:
		if (LastConfirmedMoveDirection != EMovementDirection::UP) {
			MovementVector.X = -MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::DOWN;
		}
		else {
			MovementVector.X = MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::UP;
		}
		break;
	case EMovementDirection::RIGHT:
		if (LastConfirmedMoveDirection != EMovementDirection::LEFT) {
			MovementVector.Y = MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::RIGHT;
		}
		else {
			MovementVector.Y = -MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::LEFT;
		}
		break;
	case EMovementDirection::LEFT:
		if (LastConfirmedMoveDirection != EMovementDirection::RIGHT) {
			MovementVector.Y = -MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::LEFT;
		}
		else {
			MovementVector.Y = MovementSpeed;
			LastConfirmedMoveDirection = EMovementDirection::RIGHT;
		}
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(FVector(PrevLocation.X, PrevLocation.Y, 0));
		if (CurrentElement->IsHidden()) {
			
		}
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ACPP_SnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor*OtherActor)
{
	if (IsValid(OverlappedBlock)) {
		int32 ElemIndex;
		bool bIsFirst = SnakeElements.Find(OverlappedBlock, ElemIndex);
		bool bIsFIrst = (ElemIndex == 0);
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

