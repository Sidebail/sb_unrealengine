// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodSpawner.generated.h"

//Forward forwarding for Food class, that we are going to spawn
class AFood;

UCLASS()
class SNAKECPP_API AFoodSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodSpawner();

	//Setting the class of the Food element
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	//Setting the X and Y boundaries, where we are going to spawn food items
	UPROPERTY(EditDefaultsOnly)
		int32 XBound;
	UPROPERTY(EditDefaultsOnly)
		int32 YBound;

	//Spawning the food in random coordinates within XBound and Y bound
	UFUNCTION(BlueprintCallable)
		void SpawnNewFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
