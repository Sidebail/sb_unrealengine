// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	RIGHT,
	LEFT
};

UCLASS()
class SNAKECPP_API ACPP_SnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_SnakeBase();

	/* Handle to manage the timer */
	FTimerHandle MoveTimerHandle;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;


	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		int StartSnakeSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementTimeInterval; //in seconds

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	EMovementDirection LastConfirmedMoveDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElemetsNum = 1);
	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor* OtherActor);

};
