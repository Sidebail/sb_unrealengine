// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"
#include "Food.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}


// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	AFoodSpawner::SpawnNewFood();
	
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::SpawnNewFood()
{
	int32 XCoord = (rand() % (XBound * 2)) - XBound;
	int32 YCoord = (rand() % (YBound * 2)) - YBound;

	FVector SpawnLocation(XCoord, YCoord, 0);
	FTransform SpawnTransform(SpawnLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, SpawnTransform);
 	NewFood->foodCreator = this;
	NewFood->ToggleCollision();
}


