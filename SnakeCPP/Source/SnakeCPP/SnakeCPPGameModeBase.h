// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeCPPGameModeBase.generated.h"

/**
 * 
 */
class AFoodSpawner;
UCLASS()
class SNAKECPP_API ASnakeCPPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
		AFoodSpawner* FoodCreator;
	
};
