// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKECPP_CPP_PlayerPawnBase_generated_h
#error "CPP_PlayerPawnBase.generated.h already included, missing '#pragma once' in CPP_PlayerPawnBase.h"
#endif
#define SNAKECPP_CPP_PlayerPawnBase_generated_h

#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_SPARSE_DATA
#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerHorizontalInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerVerticalInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerHorizontalInput(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->HandlePlayerVerticalInput(Z_Param_value); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_PlayerPawnBase(); \
	friend struct Z_Construct_UClass_ACPP_PlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(ACPP_PlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(ACPP_PlayerPawnBase)


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACPP_PlayerPawnBase(); \
	friend struct Z_Construct_UClass_ACPP_PlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(ACPP_PlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(ACPP_PlayerPawnBase)


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPP_PlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_PlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_PlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_PlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_PlayerPawnBase(ACPP_PlayerPawnBase&&); \
	NO_API ACPP_PlayerPawnBase(const ACPP_PlayerPawnBase&); \
public:


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_PlayerPawnBase(ACPP_PlayerPawnBase&&); \
	NO_API ACPP_PlayerPawnBase(const ACPP_PlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_PlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_PlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_PlayerPawnBase)


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_12_PROLOG
#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_RPC_WRAPPERS \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_INCLASS \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKECPP_API UClass* StaticClass<class ACPP_PlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeCPP_Source_SnakeCPP_CPP_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
