// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKECPP_FoodSpawner_generated_h
#error "FoodSpawner.generated.h already included, missing '#pragma once' in FoodSpawner.h"
#endif
#define SNAKECPP_FoodSpawner_generated_h

#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_SPARSE_DATA
#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnNewFood) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnNewFood(); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnNewFood) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnNewFood(); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodSpawner(); \
	friend struct Z_Construct_UClass_AFoodSpawner_Statics; \
public: \
	DECLARE_CLASS(AFoodSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(AFoodSpawner)


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFoodSpawner(); \
	friend struct Z_Construct_UClass_AFoodSpawner_Statics; \
public: \
	DECLARE_CLASS(AFoodSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(AFoodSpawner)


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodSpawner(AFoodSpawner&&); \
	NO_API AFoodSpawner(const AFoodSpawner&); \
public:


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodSpawner(AFoodSpawner&&); \
	NO_API AFoodSpawner(const AFoodSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodSpawner)


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_12_PROLOG
#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_RPC_WRAPPERS \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_INCLASS \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_INCLASS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_FoodSpawner_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKECPP_API UClass* StaticClass<class AFoodSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeCPP_Source_SnakeCPP_FoodSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
