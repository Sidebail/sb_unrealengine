// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeCPP/SnakeCPPGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeCPPGameModeBase() {}
// Cross Module References
	SNAKECPP_API UClass* Z_Construct_UClass_ASnakeCPPGameModeBase_NoRegister();
	SNAKECPP_API UClass* Z_Construct_UClass_ASnakeCPPGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_SnakeCPP();
	SNAKECPP_API UClass* Z_Construct_UClass_AFoodSpawner_NoRegister();
// End Cross Module References
	void ASnakeCPPGameModeBase::StaticRegisterNativesASnakeCPPGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ASnakeCPPGameModeBase_NoRegister()
	{
		return ASnakeCPPGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeCPPGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodCreator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoodCreator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeCPP,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "SnakeCPPGameModeBase.h" },
		{ "ModuleRelativePath", "SnakeCPPGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::NewProp_FoodCreator_MetaData[] = {
		{ "Category", "SnakeCPPGameModeBase" },
		{ "ModuleRelativePath", "SnakeCPPGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::NewProp_FoodCreator = { "FoodCreator", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeCPPGameModeBase, FoodCreator), Z_Construct_UClass_AFoodSpawner_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::NewProp_FoodCreator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::NewProp_FoodCreator_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::NewProp_FoodCreator,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeCPPGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::ClassParams = {
		&ASnakeCPPGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeCPPGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeCPPGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeCPPGameModeBase, 2943756115);
	template<> SNAKECPP_API UClass* StaticClass<ASnakeCPPGameModeBase>()
	{
		return ASnakeCPPGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeCPPGameModeBase(Z_Construct_UClass_ASnakeCPPGameModeBase, &ASnakeCPPGameModeBase::StaticClass, TEXT("/Script/SnakeCPP"), TEXT("ASnakeCPPGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeCPPGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
