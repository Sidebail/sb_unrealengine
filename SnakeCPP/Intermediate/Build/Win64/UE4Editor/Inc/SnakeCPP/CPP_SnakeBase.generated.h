// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef SNAKECPP_CPP_SnakeBase_generated_h
#error "CPP_SnakeBase.generated.h already included, missing '#pragma once' in CPP_SnakeBase.h"
#endif
#define SNAKECPP_CPP_SnakeBase_generated_h

#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_SPARSE_DATA
#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedBlock); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOverlap(Z_Param_OverlappedBlock,Z_Param_OtherActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddSnakeElement) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ElemetsNum); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddSnakeElement(Z_Param_ElemetsNum); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedBlock); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOverlap(Z_Param_OverlappedBlock,Z_Param_OtherActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddSnakeElement) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ElemetsNum); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddSnakeElement(Z_Param_ElemetsNum); \
		P_NATIVE_END; \
	}


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_SnakeBase(); \
	friend struct Z_Construct_UClass_ACPP_SnakeBase_Statics; \
public: \
	DECLARE_CLASS(ACPP_SnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(ACPP_SnakeBase)


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_INCLASS \
private: \
	static void StaticRegisterNativesACPP_SnakeBase(); \
	friend struct Z_Construct_UClass_ACPP_SnakeBase_Statics; \
public: \
	DECLARE_CLASS(ACPP_SnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeCPP"), NO_API) \
	DECLARE_SERIALIZER(ACPP_SnakeBase)


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPP_SnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_SnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_SnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_SnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_SnakeBase(ACPP_SnakeBase&&); \
	NO_API ACPP_SnakeBase(const ACPP_SnakeBase&); \
public:


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_SnakeBase(ACPP_SnakeBase&&); \
	NO_API ACPP_SnakeBase(const ACPP_SnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_SnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_SnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_SnakeBase)


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_PRIVATE_PROPERTY_OFFSET
#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_19_PROLOG
#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_RPC_WRAPPERS \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_INCLASS \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_PRIVATE_PROPERTY_OFFSET \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_SPARSE_DATA \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_INCLASS_NO_PURE_DECLS \
	SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKECPP_API UClass* StaticClass<class ACPP_SnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeCPP_Source_SnakeCPP_CPP_SnakeBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::RIGHT) \
	op(EMovementDirection::LEFT) 

enum class EMovementDirection;
template<> SNAKECPP_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
