// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeCPP/FoodSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodSpawner() {}
// Cross Module References
	SNAKECPP_API UClass* Z_Construct_UClass_AFoodSpawner_NoRegister();
	SNAKECPP_API UClass* Z_Construct_UClass_AFoodSpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeCPP();
	SNAKECPP_API UFunction* Z_Construct_UFunction_AFoodSpawner_SpawnNewFood();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKECPP_API UClass* Z_Construct_UClass_AFood_NoRegister();
// End Cross Module References
	void AFoodSpawner::StaticRegisterNativesAFoodSpawner()
	{
		UClass* Class = AFoodSpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnNewFood", &AFoodSpawner::execSpawnNewFood },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//Spawning the food in random coordinates within XBound and Y bound\n" },
		{ "ModuleRelativePath", "FoodSpawner.h" },
		{ "ToolTip", "Spawning the food in random coordinates within XBound and Y bound" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFoodSpawner, nullptr, "SpawnNewFood", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFoodSpawner_SpawnNewFood()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFoodSpawner_SpawnNewFood_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFoodSpawner_NoRegister()
	{
		return AFoodSpawner::StaticClass();
	}
	struct Z_Construct_UClass_AFoodSpawner_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_YBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_XBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoodClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodSpawner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeCPP,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFoodSpawner_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFoodSpawner_SpawnNewFood, "SpawnNewFood" }, // 2861769870
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodSpawner_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodSpawner.h" },
		{ "ModuleRelativePath", "FoodSpawner.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodSpawner_Statics::NewProp_YBound_MetaData[] = {
		{ "Category", "FoodSpawner" },
		{ "ModuleRelativePath", "FoodSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AFoodSpawner_Statics::NewProp_YBound = { "YBound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodSpawner, YBound), METADATA_PARAMS(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_YBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_YBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodSpawner_Statics::NewProp_XBound_MetaData[] = {
		{ "Category", "FoodSpawner" },
		{ "Comment", "//Setting the X and Y boundaries, where we are going to spawn food items\n" },
		{ "ModuleRelativePath", "FoodSpawner.h" },
		{ "ToolTip", "Setting the X and Y boundaries, where we are going to spawn food items" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AFoodSpawner_Statics::NewProp_XBound = { "XBound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodSpawner, XBound), METADATA_PARAMS(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_XBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_XBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodSpawner_Statics::NewProp_FoodClass_MetaData[] = {
		{ "Category", "FoodSpawner" },
		{ "Comment", "//Setting the class of the Food element\n" },
		{ "ModuleRelativePath", "FoodSpawner.h" },
		{ "ToolTip", "Setting the class of the Food element" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AFoodSpawner_Statics::NewProp_FoodClass = { "FoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodSpawner, FoodClass), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_FoodClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodSpawner_Statics::NewProp_FoodClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFoodSpawner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodSpawner_Statics::NewProp_YBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodSpawner_Statics::NewProp_XBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodSpawner_Statics::NewProp_FoodClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodSpawner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodSpawner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodSpawner_Statics::ClassParams = {
		&AFoodSpawner::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AFoodSpawner_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AFoodSpawner_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodSpawner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodSpawner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodSpawner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodSpawner, 1008966466);
	template<> SNAKECPP_API UClass* StaticClass<AFoodSpawner>()
	{
		return AFoodSpawner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodSpawner(Z_Construct_UClass_AFoodSpawner, &AFoodSpawner::StaticClass, TEXT("/Script/SnakeCPP"), TEXT("AFoodSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
