// HW18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template<class T>
class Stack
{
private:
    // variable for a stack
    int stackSize = 0;
    T* tempP = new T[stackSize];
    T* p = new T[stackSize];


public:
    // Empty Constructor
    Stack() {};

    // Function that pushes the new item and shifts array
    void push(T x) {
        tempP = p;
        extendStack();
        p[0] = x;
        for (int i = 0; i < stackSize; i++) {
            p[i + 1] = tempP[i];
        }
    }

    // Function that returns the last item in the stack (top item), deletes it from the array and shifts it
    T pop() {

        T itemToReturn = p[0];
        tempP = p;
        shortenStack();
        for (int i = 0; i < stackSize; i++) {
            p[i] = tempP[i+1];
        }

        return itemToReturn;
    }

    // Dynamic resize
    void extendStack() {
        stackSize++;
        p = new T[stackSize];
    }

    void shortenStack() {
        if (stackSize != 0) {
            stackSize--;
            p = new T[stackSize];
        }
    }
   
    //Get the stack item by index
    auto get(int index) {
        return p[index];
    }

    // Return the size of the stack
    int getSize() {
        return stackSize;
    }

    // Print stack to console
    void printToConsole() {
        for (int i = 0; i < stackSize; i++) {
            std::cout << p[i] << " ";
        }
        std::cout << std::endl;
    }

    // Transfer the array to a stack. I just decided it would be handy. Works bad tho :(
    template<size_t N>
    void arrayToStack(T (&arr)[N]) {
        delete p;
        stackSize = 0;
        for (T element : arr) {
            push(element);
        }
    }

};

int main()
{
    // Testing with integers
    Stack<int> steck;


    steck.push(1);
    steck.push(2);
    steck.push(3);
    
    steck.printToConsole();
 
    std::cout << "Testing Pop: " << steck.pop() << std::endl;
    
    steck.printToConsole();

    int x[5] = { 1,2,3,4,5 };

    steck.arrayToStack(x);

    steck.printToConsole();

    //testing with chars
    Stack<char> charStack;

    charStack.push('a');
    charStack.push('b');
    charStack.push('c');

    charStack.printToConsole();

    std::cout << "Testing Pop: " << charStack.pop() << std::endl;

    charStack.printToConsole();

    char y[5] = { 's','k','i','l','l'};

    charStack.arrayToStack(y);

    charStack.printToConsole();

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
