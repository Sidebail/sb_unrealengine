// HW19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal {
private:

public:
    virtual void Voice() {
        std::cout << "Abstract Speech of an abstract animal!\n";
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meow!\n";
    }
};

class Deer : public Animal {
public:
    void Voice() override {
        std::cout << "Da kto voobsche budet igrat v etot DOOM Eternal? Interfeys uzhasen, provalnaya igra!!\n";
    }
};



int main()
{
    Animal* zoo[5]; // sizeof(zoo) ������ ������ 20, ���� � �������� 4. ��� ��-�� ���� ��� ������ �� ��������, � ����������?
    Dog* dog1 = new Dog();
    Cat* cat1 = new Cat();
    Deer* commentatorIgromanii = new Deer();
    Deer* commentatorStopGame = new Deer();
    Deer* commentatorDTF = new Deer();
    zoo[0] = dog1;
    zoo[1] = cat1;
    zoo[2] = commentatorIgromanii;
    zoo[3] = commentatorStopGame;
    zoo[4] = commentatorDTF;

    for (int i = 0; i <= 4;  i++) {
        zoo[i]->Voice();
        
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
