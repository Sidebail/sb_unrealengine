// HW17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class SoSvoimiDannymi 
{
private:
    int a = 1, b = 2, c = 3;
    char z = 'z';

public:
    int getA() { return a; }
    int getB() { return b; };
    int getC() { return c; };
    char getZ() { return z; };
};

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector()
    {}
    Vector(double newX, double newY, double newZ) : x(newX), y(newY), z(newZ)
    {}
    void PrintOut() 
    {
    std::cout << "Vector: " << x << ',' << y << ',' << z << std::endl;
    }
    
    /* Get vector module/length */
    double Module() { return sqrt(x * x + y * y + z * z);}


};
int main()
{
    SoSvoimiDannymi test;

    std::cout << test.getA() << " " << test.getB() << " " << test.getC() 
        << " " << test.getZ() << std::endl;

    Vector newPos(5, 100, 20);

    std::cout << "newPos length = " << newPos.Module() << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
