using UnrealBuildTool;

public class SB_HomeProjectTarget : TargetRules
{
	public SB_HomeProjectTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("SB_HomeProject");
	}
}
